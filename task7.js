
let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function createList(newArray, parent = document.body) {
  const ul = document.createElement("ul");
  const elementList = newArray
    .map((element) => {
      return `<li>${element}</li>`;
    })
    .join("");
  ul.innerHTML = elementList;
  document.body.prepend(ul);
}
createList(array);
